package com.sagargv.phoneinputdevice;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    Camera mCamera;
    InputServer mInputServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupCamera();
        try {
            int port = 56123;
            mInputServer = new InputServer(port);
            mInputServer.start();

            WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
            String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
            Log.i("WebSocket", "Started WebSocket server on " + ip);

            TextView serverIpView = (TextView) findViewById(R.id.serverTextView);
            serverIpView.setText("ws://"+ip+":"+port);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
        try {
            mInputServer.closeAll();
            mInputServer.stop();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        mInputServer = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (grantResults.length > 1 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Log.i("TAG", "Got camera permission");
            setupCamera();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupCamera() {
        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.FLASHLIGHT) == PackageManager.PERMISSION_GRANTED ) {
            try {
                if (mCamera == null) {
                    mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    CameraPreview preview = (CameraPreview) findViewById(R.id.camera_preview);
                    preview.setCamera(mCamera);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.FLASHLIGHT}, 0);
        }
    }

    public void releaseCamera() {
        CameraPreview preview = (CameraPreview) findViewById(R.id.camera_preview);
        preview.releaseCamera();
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    public void setLum(double val) {
        DecimalFormat df = new DecimalFormat("#.####");
        String msg = "Avg. Y: " + df.format(val);

        TextView lumView = (TextView) findViewById(R.id.lumTextView);
        if (lumView != null) {
            lumView.setText(msg);
        }
        if (mInputServer != null) {
            mInputServer.sendToAll(msg);
        }
    }

    public void onFlashButton(View v) {
        if (mCamera != null) {
            Camera.Parameters p = mCamera.getParameters();
            Log.d("TAG", p.getFlashMode());
            if (p.getFlashMode().equals(Camera.Parameters.FLASH_MODE_TORCH)) {
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }
            else {
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            }
            mCamera.setParameters(p);
        }
    }
}
