package com.sagargv.phoneinputdevice;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.IOException;

/**
 * Created by sagar on 18/1/16.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {
    protected Camera mCamera;
    protected SurfaceHolder mHolder;
    protected Context mContext;

    public CameraPreview(Context context) {
        super(context);
        initCameraPreview(context);
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCameraPreview(context);
    }

    public CameraPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initCameraPreview(context);
    }

    public CameraPreview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        initCameraPreview(context);
    }

    protected void initCameraPreview(Context context) {
        mContext = context;
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void setCamera(Camera camera) {
        try {
            mCamera = camera;
            fixCameraOrientation();
            mCamera.setPreviewDisplay(mHolder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void releaseCamera() {
        if (mCamera != null) {
            try {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mCamera.setPreviewDisplay(null);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        mCamera = null;
    }

    protected void fixCameraOrientation() {
        if(mCamera != null) {
            Camera.Parameters parameters = mCamera.getParameters();
            Display display = ((WindowManager) mContext.getSystemService(mContext.WINDOW_SERVICE)).getDefaultDisplay();

            if (display.getRotation() == Surface.ROTATION_0) {
                mCamera.setDisplayOrientation(90);
            }

            if (display.getRotation() == Surface.ROTATION_270) {
                mCamera.setDisplayOrientation(180);
            }

            mCamera.setParameters(parameters);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        fixCameraOrientation();
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d("TAG", "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        double sum = 0;
        for(int i = 0; i < data.length; i += 3) {
            int Y = data[i] & 0xFF;
            sum += Y;
        }
        sum = sum / (255.0 * (data.length/3));

        MainActivity activity = (MainActivity) mContext;
        activity.setLum(sum);
    }
}
